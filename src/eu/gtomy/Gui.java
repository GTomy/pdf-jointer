package eu.gtomy;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.apache.pdfbox.multipdf.PDFMergerUtility;

import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class Gui extends JFrame {

	private JPanel contentPane;
	private JPanel panel;
	private JTextField nameField;
	private JTextField pathField;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Gui frame = new Gui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Gui() {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		double width = screenSize.getWidth();
		double height = screenSize.getHeight();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds((int)(width - 644) / 2, (int) (height - 206) / 2, 660, 217);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JButton btnNewButton = new JButton("Join all in directory");
		btnNewButton.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				try{
					String finalFile = ((pathField.getText().endsWith("/") | pathField.getText().endsWith("\\")) ? pathField.getText() : (pathField.getText() + "/")) + (nameField.getText().contains(".pdf") ? nameField.getText() : nameField.getText() + ".pdf");
					File folder = new File(pathField.getText());
					
					if(new File(finalFile).exists()){
						int dialogButton = JOptionPane.YES_NO_OPTION;
						int dialogResult = JOptionPane.showConfirmDialog (null, "Soubor již existuje, chcete tento soubor přepsat?","Warning",dialogButton);
						if(dialogResult != JOptionPane.YES_OPTION){
							return;
						}
						
						new File(finalFile).delete();
					}
					
					if(folder.isDirectory()){
						PDFMergerUtility ut = new PDFMergerUtility();
						for(File file : folder.listFiles()){
							if(file.getName().contains(".pdf")){
								System.out.println("Adding file: " + file.getName());
								ut.addSource(file);
							}
						}
						System.out.println("Starting merge...");
						ut.setDestinationFileName(finalFile);
						ut.mergeDocuments();
						System.out.println("Merge done!");
					}
				}catch(Exception e1){
					e1.printStackTrace();
				}
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 45));
		contentPane.add(btnNewButton, BorderLayout.SOUTH);
		
		panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		
		nameField = new JTextField();
		nameField.setToolTipText("Jméno výsledného souboru");
		nameField.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panel.add(nameField);
		nameField.setColumns(40);
		
		pathField = new JTextField();
		pathField.setToolTipText("Cesta do složky s pdf");
		pathField.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panel.add(pathField);
		pathField.setColumns(40);
	}

}
